﻿using System;
namespace MarketStore
{
    class BronzeFactory : MarketStore
    {
        private double turnover;

        public BronzeFactory(double turnover)
        {
            this.turnover = turnover;
        }

        public override CreditCard CreateCard()
        {
            return new BronzeCreditCard(turnover);
        }
    }
}
