﻿using System;
namespace MarketStore
{
    class SilverFactory : MarketStore
    {
        private double turnover;

        public SilverFactory(double turnover)
        {
            this.turnover = turnover;
        }

        public override CreditCard CreateCard()
        {
            return new SilverCreditCard(turnover);
        }
    }
}
