﻿using System;
namespace MarketStore
{
    class SilverCreditCard : CreditCard
    {
        private double turnover;
        private double discountRate;

        public SilverCreditCard(double turnover)
        {
            this.turnover = turnover;
            this.discountRate = 2;
        }

        public override double Turnover { get => turnover; set => turnover = value; }
        public override double DiscountRate
        {
            get
            {
                if (Turnover > 300)
                {
                    discountRate = 3.5;
                }

                return discountRate;
            }
        }


    }
}
