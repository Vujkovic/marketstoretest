﻿using System;
namespace MarketStore
{
    class GoldCreditCard : CreditCard
    {
        private double turnover;
        private double discountRate;

        public GoldCreditCard(double turnover)
        {
            this.turnover = turnover;
            this.discountRate = 2;
        }

        public override double Turnover { get => turnover; set => turnover = value; }
        public override double DiscountRate
        {
            get
            {
                int growth = (int)Math.Ceiling(Turnover / 100);
                double discountRateBonus = discountRate + growth;
                if (discountRateBonus > 10)
                {
                    discountRateBonus = 10;
                }
                return discountRateBonus;
            }
        }
    }
}
