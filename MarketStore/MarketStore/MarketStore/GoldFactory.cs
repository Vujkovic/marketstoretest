﻿using System;
namespace MarketStore
{
    class GoldFactory : MarketStore
    {
        private double turnover;

        public GoldFactory(double turnover)
        {
            this.turnover = turnover;
        }

        public override CreditCard CreateCard()
        {
            return new GoldCreditCard(turnover);
        }
    }
}
