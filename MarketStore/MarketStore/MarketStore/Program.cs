﻿using System;

namespace MarketStore
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            MarketStore bronzeFactory = new BronzeFactory(0);
            CreditCard bronze = bronzeFactory.CreateCard();
            bronze.Purchase(150);

            MarketStore silverFactory = new SilverFactory(600);
            CreditCard silver = silverFactory.CreateCard();
            silver.Purchase(850);

            MarketStore goldFactory = new GoldFactory(1500);
            CreditCard gold = goldFactory.CreateCard();
            gold.Purchase(1300);
        }
    }
}
