﻿using System;
namespace MarketStore
{
    class BronzeCreditCard : CreditCard
    {
        private double turnover;
        private double discountRate;

        public BronzeCreditCard(double turnover)
        {
            this.turnover = turnover;
            this.discountRate = 0;
        }

        public override double Turnover { get => turnover; set => turnover = value; }
        public override double DiscountRate
        {
            get
            {
                if (Turnover < 100)
                {
                    discountRate = 0;
                }
                else if (Turnover > 100 && Turnover < 300)
                {
                    discountRate = 1;
                }
                else
                {
                    discountRate = 2.5;
                }

                return discountRate;

            }
        }


    }
}
