﻿using System;
namespace MarketStore
{
    abstract class CreditCard
    {
        public abstract double Turnover { get; set; }
        public abstract double DiscountRate { get; }
        public void Purchase(double purchaseValue)
        {
            try
            {
                double discount = purchaseValue * DiscountRate / 100;
                double total = purchaseValue - discount;
                Console.WriteLine("Purchase value: ${0}", purchaseValue);
                Console.WriteLine("Discount rate: {0}%", DiscountRate);
                Console.WriteLine("Discount: ${0}", discount);
                Console.WriteLine("Total: ${0}", total);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new Exception("Purchase method exception", e);
            }
        }
    }
}
